#include <Thermocouple.h>
#include <MAX6675_Thermocouple.h>

#define piezoPin D1
#define ledPin D6

/*
  Average Thermocouple

  Reads a temperature from a thermocouple based
  on the MAX6675 driver and displays it in the default Serial.

  https://github.com/YuriiSalimov/MAX6675_Thermocouple

  Created by Yurii Salimov, May, 2019.
  Released into the public domain.
*/

#define SCK_PIN D5
#define CS_PIN  D7
#define SO_PIN  D8

Thermocouple* thermocouple;

// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(9600);

  thermocouple = new MAX6675_Thermocouple(SCK_PIN, CS_PIN, SO_PIN);

  pinMode(piezoPin, OUTPUT);//buzzer
  pinMode(ledPin, OUTPUT);//led indicator when beeping

}

// the loop function runs over and over again forever
void loop() {
  // Reads temperature
  const double celsius = thermocouple->readCelsius();
  const double kelvin = thermocouple->readKelvin();
  const double fahrenheit = thermocouple->readFahrenheit();

  // Output of information
  Serial.print("Temperature: ");
  Serial.print(celsius);
  Serial.print(" C, ");
  Serial.print(kelvin);
  Serial.print(" K, ");
  Serial.print(fahrenheit);
  Serial.print(" F");

  if (fahrenheit > 85) {
    Serial.print(" !! ALERT !!");
    beep();
  }
  Serial.println("");
  delay(500); // To delay the output of information.
}

void beep() {
  digitalWrite(ledPin, HIGH);
  digitalWrite(piezoPin, HIGH);
  delay(1000);
  digitalWrite(ledPin, LOW);
  digitalWrite(piezoPin, LOW);
}