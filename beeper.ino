

#define piezoPin D1
#define ledPin D6

void setup(void)
{
  pinMode(piezoPin, OUTPUT);//buzzer
  pinMode(ledPin, OUTPUT);//led indicator when singing a note

}
void loop()
{
  //sing the tunes
  beep();
  delay(1000);
}

void beep() {
  digitalWrite(ledPin, HIGH);
  digitalWrite(piezoPin, HIGH);
  delay(1000);
  digitalWrite(ledPin, LOW);
  digitalWrite(piezoPin, LOW);
}